import {Component} from 'angular2/core';
import {OnInit} from 'angular2/core';

import {Hero} from './hero';
import {HeroDetailComponent} from './hero-detail.component';
import {HeroService} from './hero.service'

@Component({
    selector: 'my-app',
    templateUrl: '../templates/app.component.html',
    styleUrls: ['../styles/app.component.css'],
    directives: [HeroDetailComponent],
    providers: [HeroService]
})

export class AppComponent implements OnInit {
    public title = 'Tour of Heroes';
    public heroes = HEROES;
    public selectedHero: Hero;

    constructor(private _heroService: HeroService) { }

    ngOnInit() {
        this.getHeroes();
    }

    getHeroes() {
        this._heroService.getHeroes().then(heroes => this.heroes = heroes);
    }

    onSelect(hero: Hero) { this.selectedHero = hero; }
}

var HEROES: Hero[];